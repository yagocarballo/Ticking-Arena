### Demo
- [Play Now!!](http://yagocarballo.me/TickingArena)
- [Promo Video](https://www.youtube.com/watch?v=fTmYdQFSmYE)

### Title:
Ticking Arena

### Genre:
Action, Arena, Local Multiplayer

### Platform(s):
Ouya

### Target Audience:
Aimed for players with ages 12 and above, focused towards people that own an Ouya or any android-based console.

### The Big Idea/Concept:
The idea is to have between 2 to 4 players on the same screen playing together at a hot potato like game, one of the players will have a timer floating on it’s head and that player will have to throw that timer to another player before the time runs out.
The Game will have different arenas with hidden traps to make the battles more interesting and to allow the players to create strategies and increase the difficulty as the players improve.

### Unique Selling Points:
Nowadays most multiplayer games are played online, and same screen multiplayers are less and less common everyday, Ticking Arena brings the local multiplayer experience back, allowing the players to play together on the same room.

### Play Mechanic:
The idea of Ticking Arena is simple, get rid of the timer as soon as possible and escape from the other players. The controls are easy, only four buttons are needed as the player just needs to be able to move to the left or to the right and be able to jump and throw the timer to the other players.

### Game Summary:
Ticking Arena is a local party game where 2 to 4 players will be part of intense but yet simple battles where time is of the essence. The players will have different arenas to choose and every arena will have its own secrets and hidden traps to be explored and used against the other players.

### Similar Competitive Products:
Ticking Arena is strongly influenced by Tower Fall1 and Toto Temple2 and it is inspired as well by the popular Hot Potato game3 and Super Smash Bros4.

